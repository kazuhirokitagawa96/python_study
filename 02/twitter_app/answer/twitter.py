#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import json
import requests
from bottle import route, run, static_file, view


# 定数
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))
NICO_API_URL = r"http://api.search.nicovideo.jp/api/tag/"

# 公開ディレクトリの設定
@route('/public/<filepath:path>')
def static_root(filepath):
    return static_file(filepath, root=STATIC_ROOT)

# コントローラー
@route('/')
@route('/<tag>')
@route('/<tag>/')
@route('/<tag>/<lines>')
@route('/<tag>/<lines>/')
@view('index')
def index(tag='ニコニコ動画',lines=50):
    # 検索
    # print("tag=",tag)
    qy = {
        "query": tag,
        "service": ["tag_video"]
    }
    
    title = "「{0}」のニコニコタグを検索".format(tag)
    
    req = requests.post(NICO_API_URL, json.dumps(qy).encode())
    # print("json=",json.dumps(qy).encode())
    # import pdb; pdb.set_trace()
    content = req.text
    # print("content=",content)
    data = json.loads(content.split('\n')[0])
    
    # print("data=",data)
    
    return { 
        'title': title,
        'data': data.get('values')
    }


if __name__ == '__main__':
    run(host='localhost', port=8000, debug=True, reloader=True)

