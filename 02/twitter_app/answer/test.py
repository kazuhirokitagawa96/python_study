import urllib
import xml.etree.ElementTree

def getthumbinfo(video_id):
    u = urllib.urlopen('http://ext.nicovideo.jp/api/getthumbinfo/' + video_id)
    t = u.read()
    u.close()
    return t

def test(video_id):
    x = getthumbinfo(video_id)
    e = xml.etree.ElementTree.XML(x)
    status = e.get('status')
    print video_id, status
    if status == 'ok':
        thumb = list(e)[0]
        title = thumb.find('title').text
        user_id = thumb.find('user_id').text
        first_retrieve = thumb.find('first_retrieve').text
        tags = list(thumb.find('tags'))
        print first_retrieve, video_id, user_id, title
        for i in tags:
            print i.text,
        print

test('sm20289931')
test('sm202899310')