#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from bottle import route, run, static_file, view, TEMPLATES


# 定数
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))

# 公開ディレクトリの設定
@route('/public/<filepath:path>')
def static_root(filepath):
    return static_file(filepath, root=STATIC_ROOT)

# コントローラー
@route('/')
@route('/<name>')
@route('/<name>/')
@view('index')
def index(name=''):
    if not name:
        name = "World"
    import pdb; pdb.set_trace()
    title = "Hello {0}!".format(name)

    ff = "1 +1 = {0}".format(1 + 1)

    print(ff)

    return { 
        'title': title,
        'ff': ff
    }


if __name__ == '__main__':
    run(host='localhost', port=8000, debug=True, reloader=True)

