#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from bottle import route, run, static_file, view


# 定数
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.normpath(os.path.join(ROOT_DIR, "public"))

# 公開ディレクトリの設定
@route('/public/<filepath:path>')
def static_root(filepath):
    return static_file(filepath, root=STATIC_ROOT)

# コントローラー
@route('/')
@route('/<name>')
@route('/<name>/')
@view('index')
def index(name=''):
    if not name:
        name = "World"

    title = "Hello {0}!".format(name)
    
    title = title[5:]

    ff = "1 +1 = {0} {1} {2} {3} ".format(1 + 1,2 * 3,4 // 2,1)

    return { 
        'title': title,
        'ff': ff
    }


if __name__ == '__main__':
    run(host='localhost', port=8000, debug=True, reloader=True)

